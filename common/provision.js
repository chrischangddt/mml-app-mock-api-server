const checkStatus = async (client_r, route) => {
  let result = await hasProvisioned(client_r, route);
  if (result) {
    return true;
  }
  let [is_prepare, ttl] = await isPreparing(client_r, route);
  if (is_prepare) {
    await sleep(ttl);
    return checkStatus(client_r, route);
  }
  return provision(client_r, route);
};

const hasProvisioned = async (client_r, route) => {
  return await client_r.get(`${route}_provisioned`);
};

const isPreparing = async (client_r, route) => {
  const ttl = await client_r.ttl(`${route}_preparing`);
  if (ttl == -2) {
    return [false, 0];
  }
  return [true, ttl];
};

const provision = async (client_r, route) => {
  PREPARE_TIME = 1;
  PROVISIONED_DURATION = 600;

  client_r.set(`${route}_preparing`, 'running', 'EX', PREPARE_TIME);
  await sleep(PREPARE_TIME);

  client_r.set(`${route}_provisioned`, 'running', 'EX', PROVISIONED_DURATION);
};

const sleep = async (second) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
    }, second * 1000);
  });
};

module.exports = {
  checkStatus,
  hasProvisioned,
  isPreparing,
  provision,
  sleep,
};
