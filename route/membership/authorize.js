const express = require('express');
const obp = require('object-path');

const { checkStatus } = require('../../common/provision');
const router = express.Router();

router.use(function timeLog(req, res, next) {
  console.log('Time: ', Date.now(), req.body);
  next();
});

router.post('/', async (req, res) => {
  const client_r = req.app.get('client_r');

  await checkStatus(client_r, 'get_id');

  const { Captcha } = req.body;
  if (Captcha) {
    return res.status(500).json({
      Response: {
        StatusCode: 'BAD_REQUEST'
      }
    })
  }

  return res.json({
    'Response': {
      'StatusCode': 'SUCCESS',
      'Value': {
        'RedirectUrl': 'https://thirdparty.com?timestamp=1587707323564&client_id=R5ZZfuGC-7o&result=GRANT&authorization_code=hPVm46Gg5iStI1hlXTwKg7onmn_YI7gc-G2oRCSjnsQ&client_query=cXVlcnktMT12YWx1ZS0xJnF1ZXJ5LTI9dmFsdWUtMiZxdWVyeS0zPXZhbHVlLTM%3D&signature=1aedb1952cc3f4c0fa3f2cea5428e8b078bcfa49d052a171fad8523dd49a19d2&project_id=66706096-40a8-4b8c-8599-046c749355d3&identity_pool_id=59524807-6e90-4c4f-b018-ace84973b554&identity_id=96774661-87ae-4266-9bec-b2e17d9966c3'
      }
    }
  });
});

module.exports = router;
