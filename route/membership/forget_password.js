const express = require('express');
const obp = require('object-path');

const { checkStatus } = require('../../common/provision');
const router = express.Router();

router.use(function timeLog(req, res, next) {
  console.log('Time: ', Date.now(), req.body);
  next();
});

router.post('/', async (req, res) => {
  const client_r = req.app.get('client_r');

  await checkStatus(client_r, 'login_query');

  const { Captcha } = req.body;

  const captcha_exists = await client_r.get(`captcha::${Captcha}`);
  if (captcha_exists) {
    return res.status(200).json({
      Response: {
        StatusCode: 'CAPTCHA_FAILED'
      }
    });
  }
  await client_r.set(`captcha::${Captcha}`, 1);

  isCellPhone = !!obp.get(req.body, 'Login.CellPhone.Id', null);
  isUsernamePassword = !!obp.get(req.body, 'Login.UsernamePassword.Id', null);

  if (isCellPhone) {
    return cellphone_forget_password(req, res);
  }
  if (isUsernamePassword) {
    return username_forget_password(req, res);
  }
});

const username_forget_password = (req, res) => {
  const username = obp.get(req, 'body.Login.UsernamePassword.Id');
  if (username === 'username123') {
    return res.json({
      Response: {
        StatusCode: 'SUCCESS',
        Value: {
          Email: {
            Prefix: 'IJY',
            Quota: 4,
            PartialId: 'axxxn@gmail.com'
          },
          ResetToken: 'ac075099b9e35434fbf50f1d655a828ddc52e9d56d9644aa4f5021a12886064c'
        }
      }
    });
  } else {
    return res.status(500).json({
      Response: {
        StatusCode: 'UNEXPECTED_ERROR'
      }
    });
  }
};

const cellphone_forget_password = (req, res) => {
  const phone = obp.get(req, 'body.Login.CellPhone.Id');
  if (phone === '0988777777') {
    return res.json({
      Response: {
        StatusCode: 'SUCCESS', Value: {
          CellPhone: {
            Prefix: 'KEO',
            Quota: 4,
            PartialId: '098xxxx777'
          },
          ResetToken: 'c2ecce114c16803bc5ce526790f751958b020b286ae689b373d26e9a38b4dd57'
        }
      }
    });
  } else {
    return res.status(500).json({
      Response: {
        StatusCode: 'UNEXPECTED_ERROR'
      }
    });
  }
};

module.exports = router;
