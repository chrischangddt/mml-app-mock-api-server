const express = require('express');
const obp = require('object-path');

const { checkStatus } = require('../../common/provision');
const router = express.Router();

router.use(function timeLog(req, res, next) {
  console.log('Time: ', Date.now(), req.body);
  next();
});

router.post('/', async (req, res) => {
  const client_r = req.app.get('client_r');

  await checkStatus(client_r, 'upsert_login');

  const { Captcha } = req.body;
  if (Captcha) {
    return res.status(500).json({
      Response: {
        StatusCode: 'BAD_REQUEST'
      }
    })
  }

  return res.json({
    Response: {
      StatusCode: 'SUCCESS'
    }
  });
});

module.exports = router;
