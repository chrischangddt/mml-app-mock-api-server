const express = require('express');
const obp = require('object-path');

const { checkStatus } = require('../../common/provision');
const router = express.Router();

router.use(function timeLog(req, res, next) {
  console.log('Time: ', Date.now(), req.body);
  next();
});

router.post('/', async (req, res) => {
  const client_r = req.app.get('client_r');

  await checkStatus(client_r, 'get_sso');

  const { Captcha } = req.body;

  const captcha_exists = await client_r.get(`captcha::${Captcha}`);
  if (captcha_exists) {
    return res.status(200).json({
      Response: {
        StatusCode: 'CAPTCHA_FAILED',
      },
    });
  }
  await client_r.set(`captcha::${Captcha}`, 1);

  return res.json({
    Response: {
      StatusCode: 'SUCCESS',
    }
  });
});

module.exports = router;
