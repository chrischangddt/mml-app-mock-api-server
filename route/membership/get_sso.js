const express = require('express');
const obp = require('object-path');

const { checkStatus } = require('../../common/provision');
const router = express.Router();

router.use(function timeLog(req, res, next) {
  console.log('Time: ', Date.now(), req.body);
  next();
});

router.post('/', async (req, res) => {
  const client_r = req.app.get('client_r');

  await checkStatus(client_r, 'get_sso');

  const { Captcha } = req.body;
  if (Captcha) {
    return res.status(500).json({
      Response: {
        StatusCode: 'BAD_REQUEST'
      }
    })
  }

  return res.json({
    Response: {
      StatusCode: 'SUCCESS',
      Value: {
        RedirectUrl: 'https://at-ut-accounts-gw.orcahub-dev.com/accounts?timestamp=1592203458622&from=mml.app&client_id=dada18cf7bf0805b3e861ffff06f3179&client_query=eC0zcmQtbG9jYXRpb249JTdCJTIybGF0JTIyJTNBJTIyJTIyJTJDJTIybG5nJTIyJTNBJTIyJTIyJTdEJngtM3JkLXRyYWNraW5nLWlkPSZvcmlnaW5hbF91c2VyX2lkPQ%3D%3D&client_sso_query=Q29tcG9uZW50PVRISVJEX1BBUlRZX1NFUlZJQ0UmVGFyZ2V0VXJsPWh0dHBzJTNBJTJGJTJGc29tZXdoZXJlLm91dC50aGVyZQ%3D%3D&client_form_query=eyJDZWxsUGhvbmUiOiB7IkRlZmF1bHQiOiAiMDkyMDEyMzQ1NiIsICJSZWFkT25seSI6IHRydWV9fQ%3D%3D&utm_source=mml&utm_medium=mml_medium&utm_campaign=mml_campaign&signature=d86cec9c468ee755a3ef61b0ca146e184e441ece3f99f7a812f3b05dcb2aca23'
      }
    }
  });
});

module.exports = router;