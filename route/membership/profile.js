const express = require('express');
const obp = require('object-path');

const { checkStatus } = require('../../common/provision');
const router = express.Router();

router.use(function timeLog(req, res, next) {
  console.log('Time: ', Date.now(), req.body);
  next();
});

router.post('/', async (req, res) => {
  const client_r = req.app.get('client_r');

  await checkStatus(client_r, 'profile');

  const { Captcha } = req.body;
  if (Captcha) {
    return res.status(500).json({
      Response: {
        StatusCode: 'BAD_REQUEST'
      }
    })
  }

  return res.json({
    Response: {
      StatusCode: "SUCCESS",
      Value: {
        Addresses: [],
        Attribute: {},
        Email: "treelife@gmail.com",
        Emails: [],
        Gender: "Mail",
        Name: "",
        Phone: {
          CountryCode: "886",
          Number: "0908112395"
        },
        Phones: [],
        Birthday: {
          Year: 1990,
          Month: 2,
          Day: 17,
        },
      }
    }
  });
});

module.exports = router;
