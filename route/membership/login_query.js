const express = require('express');
const obp = require('object-path');

const { checkStatus } = require('../../common/provision');
const router = express.Router();

router.use(function timeLog(req, res, next) {
  console.log('Time: ', Date.now(), req.body);
  next();
});

router.post('/', async (req, res) => {
  const client_r = req.app.get('client_r');

  await checkStatus(client_r, 'login_query');

  const { Captcha } = req.body;

  const captcha_exists = await client_r.get(`captcha::${Captcha}`);
  if (captcha_exists) {
    return res.status(200).json({
      Response: {
        StatusCode: 'CAPTCHA_FAILED'
      }
    });
  }
  await client_r.set(`captcha::${Captcha}`, 1);

  isCellPhone = !!obp.get(req.body, 'Login.CellPhone.Options.Verify', null);
  isUsernamePassword = !!obp.get(req.body, 'Login.UsernamePassword.Options.Verify', null);
  isEmail = !!obp.get(req.body, 'Login.Email.Options.Verify', null)

  if (isCellPhone) {
    let verify = obp.get(req.body, 'Login.CellPhone.Options.Verify', null)
    switch (verify) {
      case 'NO_VERIFY':
        if (isCellPhone) {
          return cellphone_no_verify(req, res);
        }
        if (isUsernamePassword) {
          return username_password_no_verify(req, res);
        }
      case 'NO_VERIFY_EVEN_IF_UNSAFE':
        return cellphone_no_verify(req, res);
      case 'SEND_IF_EXISTS':
        return send_if_exists(req, res);
      case 'SEND_ALWAYS_EVEN_IF_UNSAFE':
        return send_always_even_if_unsafe(req, res);
      case 'SEND_ALWAYS':
        return send_always(req, res);
    }
  }
  if (isUsernamePassword) {
    let verify = obp.get(req.body, 'Login.UsernamePassword.Options.Verify', null)
    switch (verify) {
      case 'NO_VERIFY':
        return username_password_no_verify(req, res);
      default:
        throw new Error('error/invlalid-error-verify');
    }
  }
  if (isEmail) {
    let verify = obp.get(req.body, 'Login.Email.Options.Verify', null)
    switch (verify) {
      case 'NO_VERIFY':
        return email_no_verify(req, res);
      default:
        throw new Error('error/invlalid-error-verify');
    }
  }
});

const email_no_verify = (req, res) => {
  const email = obp.get(req, 'body.Login.Email.Id');
  if (email === 'admin@gmail.com') {
    return res.json({
      Response: {
        Authorization: {
          ClientId: "ba9ae8b3be1384c3ade16c13e3027bac",
          State: "GRANT"
        },
        StatusCode: "SUCCESS",
        Value: {
          Email: {
            Exists: true,
            Prefix: ""
          }
        }
      }
    });
  } else {
    return res.json({
      Response: {
        Authorization: {
          ClientId: "ba9ae8b3be1384c3ade16c13e3027bac",
          State: "GRANT"
        },
        StatusCode: "SUCCESS",
        Value: {
          Email: {
            Exists: false,
            Prefix: ""
          }
        }
      }
    });
  }
}
const username_password_no_verify = (req, res) => {
  const username = obp.get(req, 'body.Login.UsernamePassword.Id');
  if (username === 'username123') {
    return res.json({
      Response: {
        Authorization: {
          ClientId: "ba9ae8b3be1384c3ade16c13e3027bac",
          State: "GRANT"
        },
        StatusCode: "SUCCESS",
        Value: {
          UsernamePassword: {
            Exists: true,
            Prefix: ""
          }
        }
      }
    });
  } else {
    return res.json({
      Response: {
        Authorization: {
          ClientId: "ba9ae8b3be1384c3ade16c13e3027bac",
          State: "GRANT"
        },
        StatusCode: "SUCCESS",
        Value: {
          UsernamePassword: {
            Exists: false,
            Prefix: ""
          }
        }
      }
    });
  }
}

const cellphone_no_verify = (req, res) => {
  const phone = obp.get(req, 'body.Login.CellPhone.Id');
  if (phone === '0988777777') {
    return res.json({
      Response: {
        StatusCode: 'SUCCESS',
        Value: {
          CellPhone: {
            Prefix: 'XIM',
            Exists: true,
          }
        },
        Authorization: {
          ClientId: 'iJNImTb1IGI',
          State: 'NOT_SET',
        },
        NextStep: 'PASSWORD'
      }
    });
  } else if (phone === '0988777666') {
    return res.json({
      Response: {
        StatusCode: 'SUCCESS',
        Value: {
          CellPhone: {
            Prefix: 'XIM',
            Exists: true
          }
        },
        Authorization: {
          ClientId: 'iJNImTb1IGI',
          State: 'GRANT'
        },
        NextStep: 'OTP'
      }
    });
  } else if (phone === '0988777665') {
    return res.json({
      Response: {
        StatusCode: 'SUCCESS',
        Value: {
          CellPhone: {
            Prefix: 'XIM',
            Exists: true
          }
        },
        Authorization: {
          ClientId: 'iJNImTb1IGI',
          State: 'GRANT'
        },
        NextStep: 'FULFILL'
      }
    });
  } else if (phone === '0988777664') {
    return res.json({
      Response: {
        StatusCode: 'SUCCESS',
        Value: {
          CellPhone: {
            Prefix: 'XIM',
            Exists: true
          }
        },
        Authorization: {
          ClientId: 'iJNImTb1IGI',
          State: 'GRANT'
        },
        NextStep: 'NONE'
      }
    });
  } else if (phone == '0988777555') {
    return res.json({
      Response: {
        StatusCode: 'LOGIN_SECURITY_FAILED'
      }
    });
  } else if (phone == '0988777444') {
    // not exists
    return res.json({
      Response: {
        StatusCode: 'SUCCESS',
        Value: {
          CellPhone: {
            Prefix: 'VAO',
            Exists: false
          }
        },
        Authorization: {
          ClientId: 'VNH74Zn0tr0',
          State: 'NOT_SET'
        }
      }
    });
  } else {
    return res.status(500).json({
      Response: {
        StatusCode: 'UNEXPECTED_ERROR'
      }
    });
  }
};

const send_if_exists = (req, res) => {
  // behavior the same as no_verify
  // no_verify(req, res);
  const phone = obp.get(req, 'body.Login.CellPhone.Id');
  if (phone === '0988777666') {
    return res.json({
      Response: {
        StatusCode: 'SUCCESS',
        Value: {
          CellPhone: {
            Prefix: 'XIM',
            Exists: true,
          }
        },
        Authorization: {
          ClientId: 'iJNImTb1IGI',
          State: 'GRANT',
        }
      }
    });
  } else if (phone === '0988777665') {
    return res.json({
      Response: {
        StatusCode: 'SUCCESS',
        Value: {
          CellPhone: {
            Prefix: 'XIM',
            Exists: true,
          }
        },
        Authorization: {
          ClientId: 'iJNImTb1IGI',
          State: 'GRANT',
        }
      }
    });
  }
};

const send_always = (req, res) => {
  const phone = obp.get(req, 'body.Login.CellPhone.Id');
  if (phone === '0988777444') {
    return res.json({
      Response: {
        StatusCode: 'SUCCESS',
        Value: {
          CellPhone: {
            Prefix: 'XIM',
            Exists: false,
          }
        },
        Authorization: {
          ClientId: 'iJNImTb1IGI',
          State: 'NOT_SET',
        }
      }
    });
  }
}

const send_always_even_if_unsafe = (req, res) => {
  const phone = obp.get(req, 'body.Login.CellPhone.Id');
  if (phone === '0988777777') {
    return res.json({
      Response: {
        StatusCode: 'SUCCESS',
        Value: {
          CellPhone: {
            Prefix: 'XIM',
            Exists: false,
          }
        },
        Authorization: {
          ClientId: 'iJNImTb1IGI',
          State: 'NOT_SET',
        }
      }
    });
  }
}

module.exports = router;
