const express = require('express');
const obp = require('object-path');

const { checkStatus } = require('../../common/provision');
const router = express.Router();

router.use(function timeLog(req, res, next) {
  console.log('Time: ', Date.now());
  next();
});

router.post('/', async (req, res) => {
  client_r = req.app.get('client_r');

  await checkStatus(client_r, 'login_verify');

  const { Captcha } = req.body;

  const captcha_exists = await client_r.get(`captcha::${Captcha}`);
  if (captcha_exists) {
    return res.status(200).json({
      Response: {
        StatusCode: 'CAPTCHA_FAILED',
      },
    });
  }
  await client_r.set(`captcha::${Captcha}`, 1);

  isCellPhone = !!obp.get(req.body, 'Login.CellPhone.Code');
  isUsernamePassword = !!obp.get(req.body, 'Login.UsernamePassword.Code');
  isCellPhonePassword = !!obp.get(req.body, 'Login.CellphonePassword.Code');

  if (isCellPhone) {
    code = obp.get(req.body, 'Login.CellPhone.Code');
    switch (code) {
      case '123456':
        return res.json({
          Response: {
            StatusCode: 'UNAUTHORIZED',
          },
        });
      default:
        return res.json({
          Response: {
            StatusCode: 'SUCCESS',
            Value: {
              IdentityId: '96774661-87ae-4266-9bec-b2e17d9966c3',
              Credential: {
                IdToken:
                  'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhcGlfaHViIjp7InByb2plY3RfaWQiOiI2NjcwNjA5Ni00MGE4LTRiOGMtODU5OS0wNDZjNzQ5MzU1ZDMiLCJpZGVudGl0eV9wb29sX2lkIjoiNTk1MjQ4MDctNmU5MC00YzRmLWIwMTgtYWNlODQ5NzNiNTU0IiwiaWRlbnRpdHlfaWQiOiI5Njc3NDY2MS04N2FlLTQyNjYtOWJlYy1iMmUxN2Q5OTY2YzMiLCJvcmlnaW4iOiJjb20uY2F0aGF5Lm15cmV3YXJkcyIsInJvbGUiOiJ1c2VyIn0sInBlcm1pc3Npb24iOlsiKi8qIl0sInRpbWVzdGFtcCI6MTU4NzQ1OTgyNDI1OCwiaWF0IjoxNTg3NDU5ODI0LCJleHAiOjE1ODc0NjM0MjQsInN1YiI6ImlkX3Rva2VuIiwiYXVkIjoiYXBpX2h1YiJ9.9Q9wggfjdoDfwH6cBUs35z7RkGWweAC6vuF8QlORM3c',
              },
            },
            Authorization: {
              ClientId: 'GcTNl-GNZZQ',
              State: 'GRANT',
            },
          },
        });
    }
  }

  if (isUsernamePassword) {
    code = obp.get(req.body, 'Login.UsernamePassword.Code');
    switch (code) {
      case 'password':
        return res.json({
          Response: {
            StatusCode: 'SUCCESS',
            Value: {
              IdentityId: '96774661-87ae-4266-9bec-b2e17d9966c3',
              Credential: {
                AuthorizationCode: "",
                IdToken:
                  'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhcGlfaHViIjp7InByb2plY3RfaWQiOiI2NjcwNjA5Ni00MGE4LTRiOGMtODU5OS0wNDZjNzQ5MzU1ZDMiLCJpZGVudGl0eV9wb29sX2lkIjoiNTk1MjQ4MDctNmU5MC00YzRmLWIwMTgtYWNlODQ5NzNiNTU0IiwiaWRlbnRpdHlfaWQiOiI5Njc3NDY2MS04N2FlLTQyNjYtOWJlYy1iMmUxN2Q5OTY2YzMiLCJvcmlnaW4iOiJjb20uY2F0aGF5Lm15cmV3YXJkcyIsInJvbGUiOiJ1c2VyIn0sInBlcm1pc3Npb24iOlsiKi8qIl0sInRpbWVzdGFtcCI6MTU4NzQ1OTgyNDI1OCwiaWF0IjoxNTg3NDU5ODI0LCJleHAiOjE1ODc0NjM0MjQsInN1YiI6ImlkX3Rva2VuIiwiYXVkIjoiYXBpX2h1YiJ9.9Q9wggfjdoDfwH6cBUs35z7RkGWweAC6vuF8QlORM3c',
                RaptToken: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwZXJtaXNzaW9uIjpbIiovKiJdLCJ0aW1lc3RhbXAiOjE2MTIxNjYwMDA1NDYsImlhdCI6MTYxMjE2NjAwMCwiZXhwIjoxNjEyMTY2NjAwLCJhdWQiOiJhcGlfaHViIiwic3ViIjoicmFwdF90b2tlbiIsImFwaV9odWIiOnsicHJvamVjdF9pZCI6ImY5NTUzMDIxLTI5YTYtNGZhYi05OTU3LTMwMTc1MmViMDAyZCIsImlkZW50aXR5X3Bvb2xfaWQiOiJlOGFlNzgwNi02OTJjLTQ4NGYtYjJkOS03N2FjZTkzZTZiM2IiLCJpZGVudGl0eV9pZCI6IjNiODVhYWZmLThlYWYtNGRmYS1hZTk2LWJjNzQ4NWIyZjJhYSIsIm9yaWdpbiI6Im9yY2FodWIuY29tIiwicm9sZSI6InVzZXIifX0.MbC-iFQrRy9zUZYfepgXnlN5q2MbmHVpGIzCHyHRpNU"
              },
            },
            Authorization: {
              ClientId: 'GcTNl-GNZZQ',
              State: 'GRANT',
            },
          },
        });
      default:
        return res.json({
          Response: {
            StatusCode: 'UNAUTHORIZED',
          },
        });
    }
  }

  if (isCellPhonePassword) {
    code = obp.get(req.body, 'Login.CellphonePassword.Code');
    switch (code) {
      case 'password':
        return res.json({
          Response: {
            StatusCode: 'SUCCESS',
            Value: {
              IdentityId: '96774661-87ae-4266-9bec-b2e17d9966c3',
              Credential: {
                AuthorizationCode: "",
                IdToken:
                  'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhcGlfaHViIjp7InByb2plY3RfaWQiOiI2NjcwNjA5Ni00MGE4LTRiOGMtODU5OS0wNDZjNzQ5MzU1ZDMiLCJpZGVudGl0eV9wb29sX2lkIjoiNTk1MjQ4MDctNmU5MC00YzRmLWIwMTgtYWNlODQ5NzNiNTU0IiwiaWRlbnRpdHlfaWQiOiI5Njc3NDY2MS04N2FlLTQyNjYtOWJlYy1iMmUxN2Q5OTY2YzMiLCJvcmlnaW4iOiJjb20uY2F0aGF5Lm15cmV3YXJkcyIsInJvbGUiOiJ1c2VyIn0sInBlcm1pc3Npb24iOlsiKi8qIl0sInRpbWVzdGFtcCI6MTU4NzQ1OTgyNDI1OCwiaWF0IjoxNTg3NDU5ODI0LCJleHAiOjE1ODc0NjM0MjQsInN1YiI6ImlkX3Rva2VuIiwiYXVkIjoiYXBpX2h1YiJ9.9Q9wggfjdoDfwH6cBUs35z7RkGWweAC6vuF8QlORM3c',
                RaptToken: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwZXJtaXNzaW9uIjpbIiovKiJdLCJ0aW1lc3RhbXAiOjE2MTIxNjYwMDA1NDYsImlhdCI6MTYxMjE2NjAwMCwiZXhwIjoxNjEyMTY2NjAwLCJhdWQiOiJhcGlfaHViIiwic3ViIjoicmFwdF90b2tlbiIsImFwaV9odWIiOnsicHJvamVjdF9pZCI6ImY5NTUzMDIxLTI5YTYtNGZhYi05OTU3LTMwMTc1MmViMDAyZCIsImlkZW50aXR5X3Bvb2xfaWQiOiJlOGFlNzgwNi02OTJjLTQ4NGYtYjJkOS03N2FjZTkzZTZiM2IiLCJpZGVudGl0eV9pZCI6IjNiODVhYWZmLThlYWYtNGRmYS1hZTk2LWJjNzQ4NWIyZjJhYSIsIm9yaWdpbiI6Im9yY2FodWIuY29tIiwicm9sZSI6InVzZXIifX0.MbC-iFQrRy9zUZYfepgXnlN5q2MbmHVpGIzCHyHRpNU"
              },
            },
            Authorization: {
              ClientId: 'GcTNl-GNZZQ',
              State: 'GRANT',
            },
          },
        });
      default:
        return res.json({
          Response: {
            StatusCode: 'VERIFY_FAILED',
          },
        });
    }
  }
});

module.exports = router;
