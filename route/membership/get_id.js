const express = require('express');
const obp = require('object-path');

const { checkStatus } = require('../../common/provision');
const router = express.Router();

router.use(function timeLog(req, res, next) {
  console.log('Time: ', Date.now(), req.body);
  next();
});

router.post('/', async (req, res) => {
  const client_r = req.app.get('client_r');

  await checkStatus(client_r, 'get_id');

  const {
    Captcha,
  } = req.body;

  const captcha_exists = await client_r.get(`captcha::${Captcha}`);
  if (captcha_exists) {
    return res.status(200).json({
      Response: {
        StatusCode: 'CAPTCHA_FAILED'
      }
    });
  }
  await client_r.set(`captcha::${Captcha}`, 1);

  return res.json({
    Response: {
      StatusCode: "SUCCESS",
      Value: {
        Certificate: [],
        Credential: {
          AuthorizationCode: "",
          IdToken: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhcGlfaHViIjp7InByb2plY3RfaWQiOiJmOTU1MzAyMS0yOWE2LTRmYWItOTk1Ny0zMDE3NTJlYjAwMmQiLCJpZGVudGl0eV9wb29sX2lkIjoiZThhZTc4MDYtNjkyYy00ODRmLWIyZDktNzdhY2U5M2U2YjNiIiwiaWRlbnRpdHlfaWQiOiJjMTI2ZTcwYS1kOGU4LTQyNTItODA1MS05ZWExN2ZhYTM0ZWUiLCJvcmlnaW4iOiJvcmNhaHViLmNvbSIsInJvbGUiOiJ1c2VyIn0sInBlcm1pc3Npb24iOlsiKi8qIl0sInRpbWVzdGFtcCI6MTU5MjE4ODQxODkzMiwiaWF0IjoxNTkyMTg4NDE4LCJleHAiOjE1OTIxODg3MTgsInN1YiI6ImlkX3Rva2VuIiwiYXVkIjoiYXBpX2h1YiJ9.EVfMYXabpBSReRaZWYrmkVQwY6oiRkVZgg9l7Vi0aQo"
        },
        IdentityId: "c126e70a-d8e8-4252-8051-9ea17faa34ee"
      }
    }
  });
});

module.exports = router;
