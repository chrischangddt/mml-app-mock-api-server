const express = require('express');
const router = express.Router();

const getId = require('./get_id');
const authorize = require('./authorize');
const loginQuery = require('./login_query');
const loginVerify = require('./login_verify');
const termsAndConditions = require('./terms_and_conditions');
const profile = require('./profile');
const getSso = require('./get_sso');
const userLogins = require('./user_logins');
const upsertLogin = require('./upsert_login');
const forgetPasword = require('./forget_password');
const resetPassword = require('./reset_password');

router.use('/Authorize', authorize);
router.use('/LoginQuery', loginQuery);
router.use('/LoginVerify', loginVerify);
router.use('/TermsAndConditions', termsAndConditions);
router.use('/GetId', getId);
router.use('/Profile', profile);
router.use('/GetSso', getSso);
router.use('/UserLogins', userLogins);
router.use('/UpsertLogin', upsertLogin);
router.use('/ForgetPassword', forgetPasword);
router.use('/ResetPassword', resetPassword);

module.exports = router;
