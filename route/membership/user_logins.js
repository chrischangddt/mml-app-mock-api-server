const express = require('express');
const obp = require('object-path');

const { checkStatus } = require('../../common/provision');
const router = express.Router();

router.use(function timeLog(req, res, next) {
  console.log('Time: ', Date.now(), req.body);
  next();
});

router.post('/', async (req, res) => {
  const client_r = req.app.get('client_r');

  await checkStatus(client_r, 'user_logins');

  const {
    Captcha,
  } = req.body;

  return res.json({
    Response: {
      StatusCode: "SUCCESS",
      Value: {
        CellPhone: '0988777777',
        Username: 'mock-user'
      }
    }
  });
});

module.exports = router;
