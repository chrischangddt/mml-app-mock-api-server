const express = require('express');
const router = express.Router();

const putJob = require('./put_job');
const jobStatus = require('./job_status');

router.use('/PutJob', putJob);
router.use('/JobStatus', jobStatus);

module.exports = router;
