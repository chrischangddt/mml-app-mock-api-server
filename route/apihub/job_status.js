const express = require('express');
const obp = require('object-path');

const { checkStatus } = require('../../common/provision');
const router = express.Router();

router.use(function timeLog(req, res, next) {
  console.log('Time: ', Date.now(), req.body);
  next();
});

router.post('/', async (req, res) => {
  const client_r = req.app.get('client_r');

  await checkStatus(client_r, 'job_status');

  const {
    Captcha,
    ReceiptId
  } = req.body;

  const captcha_exists = await client_r.get(`captcha::${Captcha}`);
  if (captcha_exists) {
    return res.status(200).json({
      Response: {
        StatusCode: 'CAPTCHA_FAILED'
      }
    });
  }
  await client_r.set(`captcha::${Captcha}`, 1);

  const jobStatus = await client_r.get(`JOB_STATUS::${ReceiptId}`);

  return res.json({
    Response: {
      StatusCode: "SUCCESS",
      Value: {
        JobStatus: jobStatus,
        Output: JSON.stringify(
          {
            ClientId: "ba9ae8b3be1384c3ade16c13e3027bac",
            Context: {
              ProjectId: "f9553021-29a6-4fab-9957-301752eb002d",
              IdentityPoolId: "e8ae7806-692c-484f-b2d9-77ace93e6b3b"
            },
            Logins: {
              CellPhone: "0908112395"
            },
            Profile: {
              Update: {
                Phone: {
                  CountryCode: "886",
                  Number: "912345678"
                }
              }
            },
            Terms: {
              Update: {
                Set: [
                  {
                    Type: "ORCA"
                  }
                ]
              }
            },
            ClientAppConfig: {
              ClientAppConfigId: "ba9ae8b3be1384c3ade16c13e3027bac",
              ClientSecret: "6d5e1df0a83114fb95a54dc47bb87da310f5ac1f034d74b3431c7577dca501d0"
            },
            UpsertUserStateResponse: {
              Response: {
                StatusCode: "SUCCESS",
                Value: {
                  IdentityId: "f38c74f5-b054-4872-99db-5cd02ce36db7",
                  Credential: {
                    IdToken: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhcGlfaHViIjp7InByb2plY3RfaWQiOiJmOTU1MzAyMS0yOWE2LTRmYWItOTk1Ny0zMDE3NTJlYjAwMmQiLCJpZGVudGl0eV9wb29sX2lkIjoiZThhZTc4MDYtNjkyYy00ODRmLWIyZDktNzdhY2U5M2U2YjNiIiwiaWRlbnRpdHlfaWQiOiJmMzhjNzRmNS1iMDU0LTQ4NzItOTlkYi01Y2QwMmNlMzZkYjciLCJvcmlnaW4iOiJvcmNhaHViLmNvbSIsInJvbGUiOiJ1c2VyIn0sInBlcm1pc3Npb24iOlsiKi8qIl0sInRpbWVzdGFtcCI6MTYwMDU4MTkwNjgzMywiaWF0IjoxNjAwNTgxOTA2LCJleHAiOjE2MDExODY3MDYsInN1YiI6ImlkX3Rva2VuIiwiYXVkIjoiYXBpX2h1YiJ9.GSyWQoldHEcxR72o4w11KC_QJz6HELKIiosJve1akhY",
                    AuthorizationCode: ""
                  },
                  Certificate: []
                }
              }
            },
            ProvisionResourceStateResponse: [
              {
                Response: {
                  StatusCode: "SUCCESS"
                }
              },
              {
                Response: {
                  StatusCode: "SUCCESS",
                  Value: {
                    RedirectUrl: "http://404.html?timestamp=1600581914203&client_id=ba9ae8b3be1384c3ade16c13e3027bac&result=GRANT&authorization_code=Fu-hKMN4R9wg0xnpRP63rOhiOmUj4EEffsJCyw-Gx-o&client_query=&signature=744dcad1298ea6b4acc3a0eaf0947a05007132dafc9af0f73ce8dfabea8623a1&project_id=f9553021-29a6-4fab-9957-301752eb002d&identity_pool_id=e8ae7806-692c-484f-b2d9-77ace93e6b3b&identity_id=f38c74f5-b054-4872-99db-5cd02ce36db7"
                  }
                }
              },
              {
                Response: {
                  StatusCode: "SUCCESS",
                  Value: {
                    Phone: {
                      CountryCode: "886",
                      Number: "912345678"
                    },
                    Name: "",
                    Gender: "UNKNOWN",
                    Email: "",
                    Addresses: [],
                    Phones: [],
                    Emails: [],
                    Attribute: {}
                  }
                }
              },
              {
                Response: {
                  StatusCode: "SUCCESS",
                  Value: {
                    Latest: [
                      {
                        Version: "ver.CROSS_2020_05_08_000",
                        Type: "CROSS",
                        Title: "共銷條款",
                        Text: ""
                      },
                      {
                        Version: "ver.ORCA_2020_09_16_000",
                        Type: "ORCA",
                        Title: "小數生活條款",
                        Text: ""
                      },
                      {
                        Version: "ver.PAR_2020_03_19_000",
                        Type: "PAR",
                        Title: "特定目的 (PAR)",
                        Text: ""
                      }
                    ],
                    User: [
                      {
                        Version: "ver.ORCA_2020_09_16_000",
                        Type: "ORCA",
                        Text: "",
                        Title: ""
                      }
                    ]
                  }
                }
              }
            ]
          }
        )
      }
    }
  });
});

module.exports = router;
