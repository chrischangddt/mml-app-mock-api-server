const express = require('express');
const obp = require('object-path');
const { v4: uuidv4 } = require('uuid');

const { checkStatus, sleep } = require('../../common/provision');
const router = express.Router();

router.use(function timeLog(req, res, next) {
  console.log('Time: ', Date.now(), req.body);
  next();
});

router.post('/', async (req, res) => {
  const client_r = req.app.get('client_r');

  await checkStatus(client_r, 'put_job');

  const {
    Captcha,
  } = req.body;

  const captcha_exists = await client_r.get(`captcha::${Captcha}`);
  if (captcha_exists) {
    return res.status(200).json({
      Response: {
        StatusCode: 'CAPTCHA_FAILED'
      }
    });
  }
  await client_r.set(`captcha::${Captcha}`, 1);

  const receiptId = uuidv4();
  run_job(client_r, receiptId);

  return res.json({
    Response: {
      StatusCode: "SUCCESS",
      Value: {
        ReceiptId: receiptId
      }
    }
  });
});

const run_job = async (client_r, receiptId) => {
  runningTime = Math.floor((Math.random() * 10));
  client_r.set(`JOB_STATUS::${receiptId}`, 'RUNNING', 'EX', runningTime);
  await sleep(runningTime);
  client_r.set(`JOB_STATUS::${receiptId}`, 'SUCCEEDED');
}

module.exports = router;
