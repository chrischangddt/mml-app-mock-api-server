const express = require('express');
const router = express.Router();

const membership = require('./membership');
const grouppoint = require('./grouppoint');
const apihub = require('./apihub');
const webhook = require('./webhook');

router.use('/membership', membership);
router.use('/grouppoint', grouppoint);
router.use('/apihub', apihub);
router.use('/webhook', webhook);

module.exports = router;
