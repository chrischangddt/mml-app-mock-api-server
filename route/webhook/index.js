const express = require('express');
const router = express.Router();

const authorizeWebhook = require('./authorize_webhook');

router.use('/AuthorizeWebhook', authorizeWebhook);

module.exports = router;
