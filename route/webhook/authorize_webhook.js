const express = require('express');
const obp = require('object-path');

const router = express.Router();

router.use(function timeLog(req, res, next) {
  console.log('Time: ', Date.now(), req.body);
  next();
});

router.get('/', async (req, res) => {
  console.log(req.query);
  return res.json({
    'statue': 200
  });
});

module.exports = router;
