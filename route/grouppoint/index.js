const express = require('express');
const router = express.Router();

const newWallet = require('./new_wallet');
const getPoint = require('./get_point');
const getPointHistory = require('./get_point_history');
const getPointNearExpire = require('./get_point_near_expire.js');

router.use('/NewWallet', newWallet);
router.use('/GetPoint', getPoint);
router.use('/GetPointHistory', getPointHistory);
router.use('/GetPointNearExpire', getPointNearExpire);

module.exports = router;
