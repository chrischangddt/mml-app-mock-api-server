const express = require('express');
const obp = require('object-path');

const router = express.Router();

router.use(function timeLog(req, res, next) {
  console.log('Time: ', Date.now(), req.body);
  next();
});

router.post('/', async (req, res) => {
  const client_r = req.app.get('client_r');

  return res.json({
    Response: {
      StatusCode: "SUCCESS",
      Value: {
        List: [
          {
            ExpiryDate: "2021-01-25T15:59:59.999999Z",
            PointAmount: "501"
          },
          {
            ExpiryDate: "2021-01-26T15:59:59.999999Z",
            PointAmount: "144"
          },
          {
            ExpiryDate: "2021-01-26T15:59:59.999999Z",
            PointAmount: "111144"
          }
        ]
      }
    }
  });
});

module.exports = router;
