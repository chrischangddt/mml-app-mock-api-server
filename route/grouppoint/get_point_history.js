const express = require('express');
const obp = require('object-path');

const router = express.Router();

router.use(function timeLog(req, res, next) {
  console.log('Time: ', Date.now(), req.body);
  next();
});

const sleep = async (second) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
    }, second * 1000);
  });
};

const generateRandomList = (count) => {
  actions = ['generic', 'spike', 'lottery', 'login_every_day', 'annual'];

  return Array(count).fill(1).map(_ => ({
    ActionCode: actions[Math.floor(Math.random() * actions.length)],
    ActionName: actions[Math.floor(Math.random() * actions.length)],
    ActionType: 'Action',
    BusinessTime: '2021-01-22T01:42:33Z',
    ExpiryDate: '2021-01-22T01:42:33Z',
    Note: 'Just a Note and with a long asdfasdfadsfhjkqweiuyasdfkjhasdkfjhasdkfjh long long length',
    OrderCode: 'Just a Code',
    PointAmount: `${Math.random()*10 > 5 ? '' : '-'}${Math.floor(Math.random() * 100)}`,
    TransactionId: '412f0e32d4df27d658e0639129251e23',
    ValidDate: '2021-01-22T01:42:33Z'
  }));
}
router.post('/', async (req, res) => {
  const client_r = req.app.get('client_r');

  await sleep(2);

  return res.json({
    Response: {
      StatusCode: "SUCCESS",
      Value: {
        List: generateRandomList(Math.floor(Math.random() * 9))
      }
    }
  });
});

module.exports = router;
