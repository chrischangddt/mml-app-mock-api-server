const express = require('express');
const obp = require('object-path');

const router = express.Router();

router.use(function timeLog(req, res, next) {
  console.log('Time: ', Date.now(), req.body);
  next();
});

router.post('/', async (req, res) => {
  const client_r = req.app.get('client_r');

  return res.json({
    Response: {
      StatusCode: "SUCCESS",
      Value: {
          Elements: [],
          EquivalentAmount: "10100",
          MerchantName: "com.orca",
          PointBalance: "987654123",
          PointName: "小樹點",
          ProviderKey: "TreePoints",
          Status: "AVAILABLE"
      }
    }
  });
});

module.exports = router;
