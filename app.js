const express = require('express')
const app = express()
const port = 3000

const redis = require('promise-redis')();
const bodyParser = require('body-parser')

const route = require('./route');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

app.use('/', route);

app.get('/', (req, res) => res.send('Hello World!'))

const main = () => {
  // set redis
  const client_redis = redis.createClient({
    host: 'host.docker.internal',
    port: 6379
  });
  client_redis.on('error', (error) => {
    console.error(error);
  });
  app.set('client_r', client_redis);
  // start server
  app.listen(port, () =>
    console.log(`mock server listening at http://localhost:${port}`)
  );
};

main();
