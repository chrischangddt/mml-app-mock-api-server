FROM node:12

WORKDIR /workspace

COPY . .

RUN npm install

EXPOSE 3000

CMD ["node", "app.js"]
